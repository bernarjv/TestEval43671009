window.addEventListener('load', ()=>{ 

    const buttons = document.querySelectorAll("#commands li input[type='radio']");
    const contents = document.getElementById("contents");
    
    function clickButton() {
        for (let i = 0; i < buttons.length; i++) {
            if (buttons[i].checked) {
                const list = getDefinition(parseInt(buttons[i].value));
                liste(list);
                break;
            }
        }
    }
    
    function liste(list) {
    
        let html = '<h2>' + list.title + '</h2>';  html += '<ul>';
  
  for (let i = 0; i < list.items.length; i++) {
    const item = list.items[i];
  
    // Check if the item is a string or a nested dictionary
    if (typeof item === 'string') {
        html += '<li>' + item + '</li>';
    } else {
        html += '<li>' + item.title + '<ul>';
        
      for (let j = 0; j < item.items.length; j++) {
          const nestedItem = item.items[j];
          
          if (typeof nestedItem === 'string') {
              html += '<li>' + nestedItem + '</li>';
            } else {
                html += '<li>' + nestedItem.title + '<ul>';
                
                for (let k = 0; k < nestedItem.items.length; k++) {
                    const innerNestedItem = nestedItem.items[k];
                    html += '<li>' + innerNestedItem + '</li>';
                }
                
                html += '</ul></li>';
            }
        }
        
        html += '</ul></li>';
    }
}
  
  html += '</ul>';
  
  contents.innerHTML = html;
  
  
}

for(let i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener('click', clickButton);
    console.log(buttons[i]);
}

});