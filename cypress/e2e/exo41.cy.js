/// <reference types="cypress" />
describe('Tests for TP 3 ', () => {
    
    // Vous vous connectez sur la page
    beforeEach(() => {
        // Cypress starts out with a blank slate for each test
        // so we must tell it to visit our website with the `cy.visit()` command.
        // Since we want to visit the same URL at the start of all our tests,
        // we include it in our beforeEach function so that it runs before each test
        cy.visit('http://localhost:3000/Exercice-41')
      })


      it('Add 2 Element', () => {
        // We'll store our item text in a variable so we can reuse it
        const newItem2 = 'Last Input'
        const newItem = 'First Input'
    
        // Let's get the input element and use the `type` command to
        // input our new list item.
        cy.get('[id = newItem]').type(`${newItem}{enter}`)
        cy.get('[id = newItem]').type(`${newItem2}{enter}`)
    
        // Now that we've typed our new item, let's check that it actually was added to the list.
        // Since it's the newest item, it should exist as the last element in the list.
        // In addition, with the two default items, we should have a total of 2 elements in the list.

        cy.get('[id= lifo] li')
          .should('have.length', 2)
          .first()
          .should('have.text',newItem)
          cy.get('[id= lifo] li')
          .last()
          .should('have.text', newItem2)
      })

      it('pop 1 Element', () => {
          // We Add 2 elements then pop one to see if only one remains
      
        const newItem2 = 'Last Input'
        const newItem = 'First Input'
    
        cy.get('[id = newItem]').type(`${newItem}{enter}`)
        cy.get('[id = newItem]').type(`${newItem2}{enter}`)

        
        cy.get('[name = pop ]').click()
        cy.get('[id= lifo] li')
          .should('have.length', 1)
          .first()
          .should('have.text',newItem2)
      })


      it('peek Element', () => {
        // Add new element then click on peek to see if the value is written in correct area.
        const newItem = 'First Input'
    
        cy.get('[id = newItem]').type(`${newItem}{enter}`)

    
        cy.get('[name = peek ]').click()
        cy.get('[id= lifo] li')
          .should('have.length', 1)
          .first()
          .should('have.text',newItem)

          cy.get('[id= peek-area]')
          .should('have.text',newItem)
      })


      it('Vide List', () => {
        // We'll store our item text in a variable so we can reuse it
     
        const newItem = 'First Input'
    
        // Let's get the input element and use the `type` command to
        // input our new list item.
        cy.get('[id = newItem]').type(`${newItem}{enter}`)

    
        cy.get('[name = pop ]').click()
        cy.get('[id= lifo] li')
          .should('have.length', 0)
      })
})