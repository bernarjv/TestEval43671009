#!/bin/bash
NBERR=$(grep -e "^ERROR" rapport_lint_html.txt | wc -l)
NBWARN=$(grep -e "^WARNING" rapport_lint_html.txt | wc -l)
color="green"
if [[ $NBERR > 0 ]]
then 
  color="red"
  else if [[ $NBWARN > 0 ]]
  then 
    color="orange"
  fi
fi
anybadge -o -l "LINT" -v "$NBERR $NBWARN" -c "$color" -f "badge_lint.svg"